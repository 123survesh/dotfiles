#!/usr/bin/env bash
set -euo pipefail

sudo apt install stow\
            sway\
            swayidle\
            swaylock\
            waybar\
            wofi\
            kitty\
            network-manager\
            mpv\
            mako\
            pavucontrol
