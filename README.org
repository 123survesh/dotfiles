#+TITLE: Karthik's Dotfiles

* Requirements
- git
- GNU Stow

* Dependencies
Check ~packages.sh~

* Installation
*Note:* Make sure you don't have folders in this folder in your config dir, If so, backup them first. Else, stow will return error.

#+BEGIN_SRC shell
git clone https://gitlab.com/kskarthik/dotfiles
cd dotfiles
stow .
#+END_SRC
