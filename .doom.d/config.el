(load "~/.doom.d/blog")
;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Sai Karthik"
      user-mail-address "kskarthik@disroot.org")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
 (setq doom-font (font-spec :family "Fira Code" :size 16 :weight 'semi-light)
       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-moonlight)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;;  Show battery info
(display-battery-mode)
;; tab is 2 spaces
;; (defcustom tab-width 2)

(setq lsp-ui-sideline-enable nil)
(setq lsp-ui-doc-enable t)
(setq +format-with-lsp nil)
(setq projectile-project-run-cmd "npm run serve")
(setq projectile-project-search-path '(("~/my" . 2)))
;; (defcustom highlight-indent-guides-method 'column)
;; (defcustom highlight-indent-guides-character "|")

;; HOOKS
;; activate lsp in web mode
(add-hook 'web-mode-hook #'lsp-deferred)
(add-hook 'elfeed-search-mode-hook 'elfeed-update)
(add-hook 'projectile-find-file 'projectile-purge-dir-from-cache "node_modules")
;; (add-hook 'prog-mode-hook 'highlight-indent-guides-mode)

; save file when exiting evil insert, visual mode
(add-hook 'evil-insert-state-exit-hook 'save-buffer)
(add-hook 'evil-visual-state-exit-hook 'save-buffer)

;; Custom Functions
(defun paste-in-new-line ()
  "Paste text in new line below the cursor"
  (interactive)
  (evil-insert-newline-below)
  (clipboard-yank))

;; use built in smtp for outgoing mails in gnus
;;(send-mail-function 'smtpmail-send-it)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(map! :leader
      :desc "open gnus" "m" #'gnus)

(map! :leader
      :desc "rest client mode" "r" #'elfeed)

(global-set-key (kbd "M-p") 'paste-in-new-line)
